const $botoninicio = document.getElementById('botoninicio')
const $modalinicio = document.getElementById('modalinicio')
const $video = document.getElementById('penguin-sledding')
const $marker = document.getElementById('markerhelper') 

let hasplayedonetime =  false


const reproducir =async (intento) => {
   try {
     const respuesta = await $video.play()
     	    $video.volume=0;

     console.log("Correcto en el intento:",intento,{respuesta});
  } catch (error) {
    console.log('Ha ocurrido un error al reproducir el video, intento:',intento)
    console.log({ error })
  }
}



$botoninicio.addEventListener('touchstart', () => {
  $modalinicio.style.display = 'none'
  reproducir(2)
})

$botoninicio.addEventListener('click', () => {
  $modalinicio.style.display = 'none'
   reproducir(2)
})

const handlerFound = async () => {
  try {

    $marker.classList.toggle('fadeout')
    $video.volume = 1;
    $video.muted = false;
    
    if (!hasplayedonetime) {
      console.log("No se ha reproducido");
      $video.currentTime =0
    } else
    {
      try
      {
        
        $video.play()
        
      } catch (error) {
        console.log("Falló en el intento dos",{error});
      }
    }
    hasplayedonetime = true
    console.log($video.currentTime);
  } catch (error) {
    console.log({ error })
  }
}




window.addEventListener('markerFound', handlerFound)

window.addEventListener('camera-error', () => {
  alert("Ha ocurrido un error al iniciar la cámara, por favor cierra todas las ventanas e intenta de nuevo.")
})



window.addEventListener('markerLost', () => {
  console.log('Se perdió el marcador')
  try {
    $marker.classList.toggle('fadeout')
    $video.pause()
  } catch (error) {
    console.log({ error })
    console.log('Ha ocurrido un error al reproducir el video')
  }
})
